using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cofre1: MonoBehaviour
{
    public int coinValue;
    public Animator cofreabre;
    public bool cofreAbierto;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<Collider2D>().CompareTag("Player"))
        {
            
            if (Input.GetKey(KeyCode.W))
            {
                if (cofreAbierto == false)
                {
                    
                    cofreabre.SetBool("open", true);
                    contadordemonedas.SumaMoneda(coinValue);
                    cofreAbierto = true;

                }
               
            }
        }
    }
}
