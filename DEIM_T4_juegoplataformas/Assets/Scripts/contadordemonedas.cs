using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class contadordemonedas : MonoBehaviour
{
    static contadordemonedas current;
    private int monedas = 0;
    public Text ContMon;
    // Start is called before the first frame update
    void Start()
    {
        current = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public static void SumaMoneda(int monedas)
    {
       current.monedas+= monedas;
       if (current.monedas < 10) current.ContMon.text = "0" + current.monedas;
       else current.ContMon.text = current.monedas.ToString();
        
    }
}
